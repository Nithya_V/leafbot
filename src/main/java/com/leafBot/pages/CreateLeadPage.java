package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {
	{
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	private WebElement companyName;
	@Given("Enter CompanyName in the CreateLead Page as (.*)")
	public void enterCompanyNameInTheCreateLeadPage(String data) {
		append(companyName,data);
		//driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}
    
	@FindBy(how=How.ID, using="createLeadForm_firstName") 
	private WebElement fName;
	@Given("Enter Firstname in the CreateLead Page as (.*)")
	public void enterFirstnameInTheCreateLeadPage(String data) {
		append(fName,data);
		//driver.findElementById("createLeadForm_firstName").sendKeys(data);
	}
    
	@FindBy(how=How.ID, using="createLeadForm_lastName") 
	private WebElement lName;
	@Given("Enter Lastname in the CreateLead Page as (.*)")
	public void enterLastnameInTheCreateLeadPage(String data) {
		append(lName, data);
		 //driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}
	
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") 
	private WebElement SubmitButton;
	@When("I click on CreateLead Button")
	public void iClickOnCreateLeadButton() {
		click(SubmitButton);
		//driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify User is in ViewLead page")
	public void verifyUserIsInViewLeadPage() {
	    System.out.println("User in ViewLead page");
	}
	

}
