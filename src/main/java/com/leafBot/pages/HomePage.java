package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;

public class HomePage extends Annotations{ 

	/*public HomePage() {
       PageFactory.initElements(driver, this);
	} 
*/
//	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	public LoginPage clickLogout() {
		WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") 
	private WebElement crmlink;
	@Given("Click on CRMSFA")
	public void clickOnCRMSFA() {
		click(crmlink);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead") 
	WebElement eleCreateLead;
	
	@Given("click on CreateLead")
	public CreateLeadPage clickOnCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}

}







