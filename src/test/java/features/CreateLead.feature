Feature: Leaftaps Create Lead

Background: 
Given Enter the Username as <username> 
And Enter the password as <password>
When Click on the login button

Scenario Outline: TC001 Create Lead Flow
Given Click on CRMSFA
And click on CreateLead
And Enter CompanyName in the CreateLead Page as <companyname>
And Enter Firstname in the CreateLead Page as <firstname>
And Enter Lastname in the CreateLead Page as <lastname>
When I click on CreateLead Button
Then Verify User is in ViewLead page

Examples:
|username|password|companyname|firstname|lastname|
|DemoSalesManager|crmsfa|HCL|Nithya|V|
|DemoSalesManager|crmsfa|TCS|Priya|S|
